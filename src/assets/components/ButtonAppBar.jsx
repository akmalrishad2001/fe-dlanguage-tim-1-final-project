import * as React from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';

import { useNavigate } from 'react-router-dom';
import {Button} from '@mui/material'
import theme from './theme';
import Logo from '../img/logo.png'



export default function ButtonAppBar() {
  const navigate = useNavigate();
  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static" color='common' elevation={0}>
        <Toolbar>
          <Box sx={{ml:{xs:"0px",xl:"60px"}}}>
          <img src={Logo} alt='logo'></img>
          </Box> 
          <Typography theme={theme} variant="h5" component="div" sx={{ ml:'10px', flexGrow: 1,fontSize:{
              md:"24px",
              sm:"24px",
              xs:"17px",
            } }}>
            Language
          </Typography>
          <Button
          onClick={()=>navigate('/login')}
          theme={theme}
          variant='contained'
          sx={{
            // width:"86px",
            // height:"40px",
            borderRadius:"8px",
            padding:"10px 20px",
            mr:{
              md:"16px",
              sm:"16",
              xs:"10px"
            },
            mt:"23px",
            mb:"23px",
            fontSize:{
              md:"16px",
              sm:"16px",
              xs:"10px"
            }
          }}>Login</Button>
          <Button
          onClick={()=>navigate('/signup')}
          theme={theme}
          color='secondary'
          variant='contained'
          sx={{
            // width:"105px",
            // height:"40px",
            borderRadius:"8px",
            padding:"10px 20px",
            mr:{
              xs:"0px",
              sm:"50px",
              md:"50px"
            },
            color:"#fff",
            fontSize:{
              md:"16px",
              sm:"16px",
              xs:"10px"
            }
          }}>sign up</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}