import React from "react";
import ButtonAppBar from "../assets/components/ButtonAppBar";
import { Box, Button, Container, TextField, Typography } from "@mui/material";
import theme from "../assets/components/theme";

export const ResetPass = () => {
  return (
    <Box>
      <ButtonAppBar></ButtonAppBar>
      <Container
        sx={{ width: { md: "50%", sm: "80%", xs: "100%" }, mt: "60px" }}
      >
        <Typography
          fontFamily={"Montserrat"}
          fontWeight={500}
          variant="h5"
          sx={{
            fontSize: {
              md: "24px",
              sm: "24px",
              xs: "16px",
            },
            color: "#333333",
          }}
        >
          Reset Password
        </Typography>
        <Typography
          fontFamily={"Montserrat"}
          variant="subtitle1"
          sx={{
            fontSize: {
              md: "16px",
              sm: "16px",
              xs: "12px",
            },
            color: "#4F4F4F",
            mb: "60px",
            mt: "16px",
          }}
        >
          Please enter your email address
        </Typography>
        <TextField
          fullWidth
          label="Email"
          size="small"
          sx={{
            "& .MuiFormLabel-root": {
              fontFamily: "Montserrat",
            },
            mb: "24px",
          }}
        ></TextField>
        <Box display={"flex"} justifyContent={"end"}>
          <Button
            theme={theme}
            variant="contained"
            color="secondary"
            sx={{
              // width:"86px",
              //   height: "40px",
              borderRadius: "8px",
              padding: { md: "10px 40px", sm: "10px 40px", xs: "10px 20px" },
              mb: "60px",
              mr: "24px",
              color: "#fff",
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "10px",
              },
            }}
          >
            Cencel
          </Button>
          <Button
            theme={theme}
            variant="contained"
            sx={{
              // width:"86px",
              //   height: "40px",
              borderRadius: "8px",
              padding: { md: "10px 40px", sm: "10px 40px", xs: "10px 20px" },
              mb: "60px",
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "10px",
              },
            }}
          >
            Confirm
          </Button>
        </Box>
      </Container>
    </Box>
  );
};
