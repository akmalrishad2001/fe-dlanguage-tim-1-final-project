import React from "react";
import ButtonAppBar from "../assets/components/ButtonAppBar";
import { Box, Button, Container, TextField, Typography } from "@mui/material";
import theme from "../assets/components/theme";

export const NewPass = () => {
  return (
    <Box>
      <ButtonAppBar></ButtonAppBar>
      <Container
        sx={{ width: { md: "50%", sm: "80%", xs: "100%" }, mt: "60px" }}
      >
        <Typography
          fontFamily={"Montserrat"}
          fontWeight={500}
          variant="h5"
          sx={{
            fontSize: {
              md: "24px",
              sm: "24px",
              xs: "16px",
            },
            color: "#333333",
            mb: "60px",
          }}
        >
          Create Password
        </Typography>
        <TextField
          fullWidth
          label="Password"
          size="small"
          sx={{
            "& .MuiFormLabel-root": {
              fontFamily: "Montserrat",
            },
            mb: "24px",
          }}
        ></TextField>
        <TextField
          fullWidth
          label="New Password"
          size="small"
          sx={{
            "& .MuiFormLabel-root": {
              fontFamily: "Montserrat",
            },
            mb: "40px",
          }}
        ></TextField>
        <Box display={"flex"} justifyContent={"end"}>
          <Button
            theme={theme}
            variant="contained"
            color="secondary"
            sx={{
              // width:"86px",
              //   height: "40px",
              borderRadius: "8px",
              padding: { md: "10px 40px", sm: "10px 40px", xs: "10px 20px" },
              mb: "60px",
              mr: "24px",
              color: "#fff",
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "10px",
              },
            }}
          >
            Cencel
          </Button>
          <Button
            theme={theme}
            variant="contained"
            sx={{
              // width:"86px",
              //   height: "40px",
              borderRadius: "8px",
              padding: { md: "10px 40px", sm: "10px 40px", xs: "10px 20px" },
              mb: "60px",
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "10px",
              },
            }}
          >
            Login
          </Button>
        </Box>
      </Container>
    </Box>
  );
};
