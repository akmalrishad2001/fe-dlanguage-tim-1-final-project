import React from "react";
import ButtonAppBar from "../assets/components/ButtonAppBar";
import { Box, Button, Container, TextField, Typography } from "@mui/material";
import theme from "../assets/components/theme";

const Login = () => {
  return (
    <Box>
      <ButtonAppBar></ButtonAppBar>
      <Container
        sx={{ width: { md: "50%", sm: "80%", xs: "100%" }, mt: "60px" }}
      >
        <Typography
          fontFamily={"Montserrat"}
          fontWeight={500}
          variant="h5"
          sx={{
            fontSize: {
              md: "24px",
              sm: "24px",
              xs: "16px",
            },
            color: "#226957",
          }}
        >
          Welcome Back!
        </Typography>

        <Typography
          fontFamily={"Montserrat"}
          variant="subtitle1"
          sx={{
            fontSize: {
              md: "16px",
              sm: "16px",
              xs: "12px",
            },
            color: "#4F4F4F",
            mb: "40px",
            mt: "16px",
          }}
        >
          Please Login First
        </Typography>
        <TextField
          fullWidth
          label="Email"
          size="small"
          sx={{
            "& .MuiFormLabel-root": {
              fontFamily: "Montserrat",
            },
            mb: "24px",
          }}
        ></TextField>
        <TextField
          fullWidth
          label="Password"
          size="small"
          sx={{
            "& .MuiFormLabel-root": {
              fontFamily: "Montserrat",
            },
            mb: "24px",
          }}
        ></TextField>
        <Typography
          fontFamily={"Montserrat"}
          variant="subtitle1"
          sx={{
            fontSize: {
              md: "16px",
              sm: "16px",
              xs: "12px",
            },
            color: "#4F4F4F",
            mb: "40px",
          }}
        >
          Forgot Password? <a href="">Click Here</a>
        </Typography>
        <Box display={"flex"} justifyContent={"end"}>
          <Button
            theme={theme}
            variant="contained"
            sx={{
              // width:"86px",
              // height: "40px",
              borderRadius: "8px",
              padding: "10px 40px",
              mb: "60px",
              fontSize: {
                md: "16px",
                sm: "16px",
                xs: "10px",
              },
            }}
          >
            Login
          </Button>
        </Box>
        <Typography
          fontFamily={"Montserrat"}
          variant="subtitle1"
          align="center"
          sx={{
            fontSize: {
              md: "16px",
              sm: "16px",
              xs: "12px",
            },
            color: "#4F4F4F",
            mb: "40px",
          }}
        >
          Don't Have account? <a href="/signup">Sign Up here</a>
        </Typography>
      </Container>
    </Box>
  );
};

export default Login;
