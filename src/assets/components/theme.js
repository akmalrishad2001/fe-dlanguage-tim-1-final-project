import { createTheme } from "@mui/material";

const theme = createTheme({
  palette: {
    primary: {
      main: "#226957",
    },
    secondary: {
      main: "#EA9E1F",
    },
    text: {
      primary: "#FFFFFF",
    },
  },
  typography: {
    fontFamily: "Montserrat",
    fontWeightRegular: 500,
    fontSize: 16,
    button: {
      fontSize: "1rem",
    },
  },
  shape: {
    borderRadius: 8,
  },
});

export default theme;
