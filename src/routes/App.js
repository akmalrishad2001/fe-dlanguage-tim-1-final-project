import { BrowserRouter as Router, Routes, Route } from "react-router-dom";

import React from "react";
import Login from "../pages/Login";
import SignUp from "../pages/SignUp";
import { ResetPass } from "../pages/ResetPass";
import { NewPass } from "../pages/NewPass";

const App = () => {
  return (
    <Router>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/signup" element={<SignUp />} />
        <Route path="/reset-password" element={<ResetPass />} />
        <Route path="/new-password" element={<NewPass />} />
      </Routes>
    </Router>
  );
};

export default App;
